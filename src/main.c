/* Siobhan Audio Player
 *
 * (c) 2005 Ariel Rios <ariel@gnu.org>
 *
 * main.c: main program
 *
 * This file is part of Siobhan Media Player.
 *
 *   Siobhan is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Siobhan is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Siobhan; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

#define HAVE_GPE 1

#include <gst/gst.h>
#include <gtk/gtk.h>

#ifdef HAVE_GPE
#include <gpe/init.h>
#include <gpe/picturebutton.h>
#endif

#ifdef HAVE_LIBGLADE
#include <glade.h>
#endif


#include "timer.h"
#include "callback.h"
#include "video.h"

int 
main (int argc, char *argv [])
{
	GtkWidget *app;
	GtkWidget *button_play, *stop, *pause, *properties, *mute;
	GtkWidget *volume, *loop;
	GtkWidget *hbox, *vbox;
	GtkWidget *filesel;
	GtkWidget *slider;
	GtkWidget *video;
		
	GstElement *audiosink, *videosink;
	GstElement *play;

	CallBackData *data = g_new (CallBackData, 1);

	gst_init (&argc, &argv);

#ifndef HAVE_GPE
 	gtk_init (&argc, &argv);
#else
	gpe_application_init (&argc, &argv);
#endif

	play = gst_element_factory_make ("playbin", "play");
	

	/*
	  audiosink = gst_element_factory_make ("osssink", "play");
	  audiosink = gst_element_factory_make ("filesink", "play_audio");
	
	  g_object_set (G_OBJECT (audiosink), "location", "/dev/null", NULL);

	*/

	videosink = gst_element_factory_make ("ximagesink", NULL);

	g_object_set (play, "video-sink", videosink, NULL);
	
	/*
	  g_object_set (play, "audio-sink", audiosink, NULL); 
	*/

	if (argc >= 2)
	  g_object_set (G_OBJECT (play), "uri", argv[1], NULL);	
	
	app = gtk_window_new (GTK_WINDOW_TOPLEVEL);

#ifdef HAVE_GPE
	button_play = gpe_button_new_from_stock (GTK_STOCK_MEDIA_PLAY, GPE_BUTTON_TYPE_ICON);
	pause = gpe_button_new_from_stock (GTK_STOCK_MEDIA_PAUSE, GPE_BUTTON_TYPE_ICON);
	stop = gpe_button_new_from_stock (GTK_STOCK_MEDIA_STOP, GPE_BUTTON_TYPE_ICON);
	filesel = gpe_button_new_from_stock (GTK_STOCK_OPEN, GPE_BUTTON_TYPE_ICON);
	mute = gtk_button_new_with_label ("mute");

#else					  
	button_play = gtk_button_new_with_label (">");
	pause = gtk_button_new_with_label ("||");
	stop = gtk_button_new_with_label ("[]");
	filesel = gtk_button_new_from_stock (GTK_STOCK_OPEN);
	mute = gtk_button_new_with_label ("mute");

#endif 					  

	loop = gtk_check_button_new_with_label ("repeat");

	slider = gst_player_timer_new (play);

	video = gst_player_video_new (videosink, play); 

	volume = gtk_hscale_new_with_range (0, 4, 0.5);

	hbox = gtk_hbox_new (TRUE, 0);
	vbox = gtk_vbox_new (FALSE, 0);
	
	data->thread = play;
	data->widget = slider;
	data->volume = volume;
	data->loop = loop;
	

	GST_PLAYER_VIDEO (video)->eos_signal = g_signal_connect (G_OBJECT (play), "eos", G_CALLBACK (eos), data);

	gtk_window_set_title (GTK_WINDOW (app), "Siobhan Media Player");

	gtk_range_set_value (GTK_RANGE (volume), 4);

	gtk_box_pack_start (GTK_BOX (hbox), button_play, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), pause, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), stop, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), filesel, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), mute, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), loop, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), video, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), slider, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), volume, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 0);

	gtk_container_add (GTK_CONTAINER (app), vbox);
	
	g_signal_connect (G_OBJECT (app), "delete_event", G_CALLBACK (cb_delete), data);
	g_signal_connect (G_OBJECT (app), "destroy", G_CALLBACK (cb_quit), data);
	g_signal_connect (G_OBJECT (button_play), "clicked", G_CALLBACK (cb_play), data);
	g_signal_connect (G_OBJECT (pause), "clicked", G_CALLBACK (cb_pause), play);
	g_signal_connect (G_OBJECT (stop), "clicked", G_CALLBACK (cb_stop), play);
	g_signal_connect (G_OBJECT (filesel), "clicked", G_CALLBACK (cb_choose), data);
	g_signal_connect (G_OBJECT (mute), "clicked", G_CALLBACK (cb_volume_mute),  data);
	g_signal_connect (G_OBJECT (volume), "value-changed", G_CALLBACK (cb_volume), data);

	g_idle_add (cb_iterate, (gpointer) data);

	gtk_widget_show_all (app);

	gtk_main ();

	return 0;	
}
