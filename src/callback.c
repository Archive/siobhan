/* Siobhan Audio Player
 *
 * (c) 2005 Ariel Rios <ariel@gnu.org>
 *
 * callback.c: program callbacks
 *
 * This file is part of Siobhan Audio Player.
 *
 *   Siobhan is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Siobhan is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Foobar; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

#include <gst/gst.h>
#include <gtk/gtk.h>

#include "callback.h"
#include "disc.h"
#include "timer.h"

void 
eos (GstElement *src, gpointer data) 
{
  CallBackData *cbd = (CallBackData *) data;  
  GstElement *thread = GST_ELEMENT (cbd->thread);
  GtkWidget *loop_widget = GTK_WIDGET (cbd->loop);
  gboolean loop;

  g_print ("have eos, quitting\n");

  gst_element_set_state (GST_ELEMENT (thread), GST_STATE_NULL);
  
  g_object_get (G_OBJECT (loop_widget), "active", &loop, NULL);

  if (loop){
    
    g_print ("repeat file\n");
    
    gst_element_set_state (GST_ELEMENT (thread), GST_STATE_PLAYING);
  }
}

void 
set_queue_size (GstElement *play){
  
  g_object_set (play, "queue-size-bytes", (guint64) 2 * 1024 * 1024, NULL);
  
}

void
cb_play (GtkWidget *widget, gpointer data)
{

  CallBackData *cbd = (CallBackData *) data;
  GstElement *thread = GST_ELEMENT (cbd->thread);
  
  if (gst_element_get_state (GST_ELEMENT (thread)) != GST_STATE_PLAYING){

    //set_queue_size (thread);
	gst_element_set_state (thread, GST_STATE_PLAYING);
  }
}



void
cb_stop (GtkWidget *widget, gpointer data)
{

  
  GstElement *thread = GST_ELEMENT (data);

  if (gst_element_get_state (GST_ELEMENT (thread)) != GST_STATE_NULL){
    gst_element_set_state (GST_ELEMENT (thread), GST_STATE_NULL);

  }
}


void 
cb_pause (GtkWidget *widget, gpointer data)
{

    GstElement *thread = GST_ELEMENT (data);

	switch (gst_element_get_state (thread))
	{
		case GST_STATE_PLAYING:
		  g_print ("'twas playing, now paus'd \n");
		  gst_element_set_state (thread, GST_STATE_PAUSED);
			break;
		case GST_STATE_PAUSED:
		  g_print ("'twas paus'd, now playin' \n");
		  gst_element_set_state (thread, GST_STATE_PLAYING);
		  break;
		default:
			g_print ("other \n");
			break;
	}
}



void
cb_properties (GtkWidget *widget, gpointer data)
{
  
}


gint
cb_delete (GtkWidget *widget, gpointer data)
{
  return FALSE;
}


void 
cb_quit (GtkWidget *widget, gpointer data)
{

  CallBackData *cbd = (CallBackData *) data;

  GstElement *thread = GST_ELEMENT (cbd->thread);

  if (gst_element_get_state (GST_ELEMENT (thread)) != GST_STATE_NULL)
    gst_element_set_state (GST_ELEMENT (thread), GST_STATE_NULL);
  
  g_free (data);

  gtk_main_quit ();
}
 
void 
select_song (const gchar *filename, GstElement *play)
{
  g_object_set (G_OBJECT (play), "uri", filename, NULL);
  
}


void 
cb_choose (GtkWidget *widget, gpointer data)
{
	GtkWidget *selection;
	const gchar *filename;

	gchar *str = NULL;

	CallBackData *cbd = (CallBackData *) data;

	GstElement *filesrc = cbd->thread;
	
	selection = gtk_file_selection_new ("Open File");
	
	
	if (gtk_dialog_run (GTK_DIALOG (selection)) == GTK_RESPONSE_OK)
	{


	  filename = gtk_file_selection_get_filename (GTK_FILE_SELECTION (selection));

	  str = g_strdup_printf ("file://%s", filename);
	  
	  select_song (str, filesrc);
	}
	
	if (str != NULL)
	  g_free (str);
	

	gtk_widget_destroy (selection);
	
	return;
	
}


void
cb_open_disc (GtkWidget *widget,
	      gpointer   data)
{

  CallBackData *cbd = (CallBackData *) data;

  GstElement *play = GST_ELEMENT (cbd->thread);


  /* FIXME: drive detection */
  GError *error = NULL;
  CdType type = cd_detect_type ("/dev/cdrom", &error);
  
  if (type == CD_TYPE_ERROR) {
    g_error_free (error);
  } 
  else {
    const gchar *uri = NULL;
    
    switch (type) {
      case CD_TYPE_DATA:
        /* FIXME:
         * - open correct location to mount path by default.
         */
        /* cb_open_file (widget, data); */
        return;
      case CD_TYPE_CDDA:
        uri = "cdda://";
        break;
      case CD_TYPE_VCD:
        uri = "vcd://";
        break;
      case CD_TYPE_DVD:
        uri = "dvd://";
        break;
      default:
        g_assert_not_reached ();
	}
    gst_element_set_state (play, GST_STATE_READY);

    select_song (uri, play);


  }
}


gboolean
cb_iterate (gpointer data)
{


  CallBackData *cbd = (CallBackData *) data;
  GstPlayerTimer *slider = GST_PLAYER_TIMER (cbd->widget);
  GstElement *filesrc = cbd->thread;


  /*
    gint64 len, pos;
  GstFormat fmt = GST_FORMAT_TIME;

  
  if (gst_element_query (decoder, GST_QUERY_POSITION, &fmt, &pos) && gst_element_query (decoder, GST_QUERY_TOTAL, &fmt, &len)){
    g_print  ("\nTime %" GST_TIME_FORMAT " / %" GST_TIME_FORMAT " \r", 
	      GST_TIME_ARGS (pos), GST_TIME_ARGS (len));
	      }                                                                                                                                                          
  */

  g_usleep (100); 

  if (gst_element_get_state (filesrc) == GST_STATE_PLAYING)

    gst_player_timer_progress (slider);


  
  return TRUE;
}

void 
cb_volume_mute (GtkWidget *widget, gpointer data){

  CallBackData *cbd = (CallBackData *) data;
  GstElement *play = GST_ELEMENT (cbd->thread);
 
  gtk_range_set_value (GTK_RANGE (cbd->volume), 0); 
 
  g_object_set (play, "volume", (gdouble) 0, NULL);

}

void                
cb_volume (GtkWidget *widget, gpointer data){

  CallBackData *cbd = (CallBackData *) data;
  GstElement *play = GST_ELEMENT (cbd->thread);
  gdouble volumen = gtk_range_get_value (GTK_RANGE (cbd->volume));
 
  g_object_set (play, "volume", volumen, NULL);
}		     
