/* Siobhan Audio Player
 *
 * (c) 2005 Ariel Rios <ariel@gnu.org>
 *
 * callback.h: program callbacks header
 *
 * This file is part of Siobhan Audio Player.
 *
 *   Siobhan is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Siobhan is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Foobar; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 */

#include <gst/gst.h>
#include <gtk/gtk.h>

typedef struct _CallBackData {
  GtkWidget *widget;
  GtkWidget *volume;
  GtkWidget *loop;
  
  GstElement *thread;

} CallBackData;


void                cb_choose (GtkWidget *widget, gpointer data);

gint                cb_delete (GtkWidget *widget, gpointer data);

gboolean            cb_iterate (gpointer data);

void                cb_full_screen (GtkWidget *widget, gpointer data);

void                cb_full_screen_off (GtkWidget *widget, gpointer data);

void                cb_open_disc (GtkWidget *widget, gpointer data);

void                cb_pause (GtkWidget *widget, gpointer data);

void                cb_play (GtkWidget *widget, gpointer data);

void                cb_properties (GtkWidget *widget, gpointer data);

void                cb_quit (GtkWidget *widget, gpointer data);

void                cb_stop (GtkWidget *widget, gpointer data);

void                cb_volume_mute (GtkWidget *widget, gpointer data);

void                cb_volume (GtkWidget *widget, gpointer data);

void                eos (GstElement *src, gpointer data);

void                select_song (const gchar *filename, GstElement *filesrc);

void                set_queue_size (GstElement *play);
